INSERT INTO oauth_client_details (client_id, client_secret, resource_ids, scope, authorized_grant_types, access_token_validity, refresh_token_validity, autoapprove)
    SELECT client_id, client_secret, resource_ids, scope, authorized_grant_types, access_token_validity, refresh_token_validity, autoapprove FROM oauth_client_details
    UNION
    VALUES ('client_id1', '$2y$10$2otXvo/puvFNoiP3SXhFs..YTFPK79BmiY913R69Ed2kk.MpKk8TS', 'USER_CLIENT_RESOURCE,USER_ADMIN_RESOURCE', 'ROLE_ADMIN', 'password,authorization_code,refresh_token', 24000, 36000, true)
    EXCEPT
    SELECT client_id, client_secret, resource_ids, scope, authorized_grant_types, access_token_validity, refresh_token_validity, autoapprove FROM oauth_client_details;

INSERT INTO users (username, password, email, enabled, account_expired, credentials_expired, account_locked)
    SELECT username, password, email, enabled, account_expired, credentials_expired, account_locked FROM users
    UNION
    VALUES ('admin', '$2y$10$2otXvo/puvFNoiP3SXhFs..YTFPK79BmiY913R69Ed2kk.MpKk8TS', 'admin@example.com', true, false, false, false)
    EXCEPT
    SELECT username, password, email, enabled, account_expired, credentials_expired, account_locked FROM users;

INSERT INTO roles (name)
    SELECT name FROM roles
    UNION
    VALUES ('ROLE_ADMIN')
    EXCEPT
    SELECT name FROM roles;

INSERT INTO roles (name)
    SELECT name FROM roles
    UNION
    VALUES ('ROLE_USER')
    EXCEPT
    SELECT name FROM roles;

INSERT INTO users_roles (user_id, role_id)
    SELECT user_id, role_id FROM users_roles
    UNION
    VALUES (1,1)
    EXCEPT
    SELECT user_id, role_id FROM users_roles;

INSERT INTO permissions (name)
    SELECT name FROM permissions
    UNION
    VALUES ('PERMISSION_MANAGEMENT')
    EXCEPT
    SELECT name FROM permissions;

INSERT INTO permissions (name)
    SELECT name FROM permissions
    UNION
    VALUES ('PERMISSION_BASIC')
    EXCEPT
    SELECT name FROM permissions;

INSERT INTO roles_permissions (role_id, permission_id)
    SELECT role_id, permission_id FROM roles_permissions
    UNION
    VALUES (1,1), (1,2), (2,2)
    EXCEPT
    SELECT role_id, permission_id FROM roles_permissions;
