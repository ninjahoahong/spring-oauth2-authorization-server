CREATE TABLE oauth_client_details (
  client_id VARCHAR(255) PRIMARY KEY,
  client_secret VARCHAR(255) NOT NULL,
  resource_ids VARCHAR(255) NOT NULL,
  scope VARCHAR(255) DEFAULT NULL,
  authorized_grant_types VARCHAR(255) DEFAULT NULL,
  web_server_redirect_uri VARCHAR(255) DEFAULT NULL,
  authorities VARCHAR(255) DEFAULT NULL,
  access_token_validity INTEGER DEFAULT NULL,
  refresh_token_validity INTEGER DEFAULT NULL,
  additional_information VARCHAR(4096) DEFAULT NULL,
  autoapprove BOOLEAN DEFAULT FALSE
);

CREATE TABLE permissions (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(100) UNIQUE
);

CREATE TABLE roles (
     id BIGSERIAL PRIMARY KEY,
     name VARCHAR(100) UNIQUE
);

CREATE TABLE roles_permissions (
    role_id BIGINT REFERENCES roles(id) ON UPDATE CASCADE ON DELETE CASCADE,
    permission_id BIGINT REFERENCES permissions(id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT roles_permissions_pkey PRIMARY KEY (role_id, permission_id)
);

CREATE TABLE users (
    id BIGSERIAL PRIMARY KEY,
    username VARCHAR(100) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    enabled BOOLEAN NOT NULL DEFAULT FALSE,
    account_expired BOOLEAN NOT NULL,
    credentials_expired BOOLEAN NOT NULL,
    account_locked BOOLEAN NOT NULL
);

CREATE TABLE users_roles (
  user_id     BIGINT REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE,
  role_id     BIGINT REFERENCES roles (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT  users_roles_pkey PRIMARY KEY (user_id, role_id)
);

