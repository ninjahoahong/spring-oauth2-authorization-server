package com.ninjahoahong.oauthserver.security.oauth;

import com.ninjahoahong.oauthserver.jpa.UserRepository;
import com.ninjahoahong.oauthserver.model.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service(value = "userDetailsService")
public class OAuth2UserDetailsService implements UserDetailsService {

    Logger logger = LoggerFactory.getLogger(OAuth2UserDetailsService.class);

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username)
        throws UsernameNotFoundException {

        logger.debug("UserDetails loadUserByUsername() usernaem {}", username);

        User user = userRepository.findByUsername(username)
            .orElseThrow(() ->
                new BadCredentialsException("Wrong username or password: " + username)
            );

        List<GrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName().name()));
            role.getPermissions().forEach(permission ->
                authorities.add(new SimpleGrantedAuthority(permission.getName().name())));
        });

        return OAuth2UserDetails.builder()
            .id(user.getId())
            .username(user.getUsername())
            .email(user.getEmail())
            .password(user.getPassword())
            .authorities(authorities)
            .isAccountNonExpired(!user.isAccountExpired())
            .isAccountNonLocked(!user.isAccountLocked())
            .isCredentialsNonExpired(!user.isCredentialsExpired())
            .isEnabled(user.isEnabled())
            .build();
    }
}
