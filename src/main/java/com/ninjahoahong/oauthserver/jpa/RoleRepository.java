package com.ninjahoahong.oauthserver.jpa;

import com.ninjahoahong.oauthserver.model.entity.Role;
import com.ninjahoahong.oauthserver.model.entity.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}