package com.ninjahoahong.oauthserver.jpa;

import com.ninjahoahong.oauthserver.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    Optional<User> existsByUsername(String username);
    Optional<User> existsByEmail(String email);
}