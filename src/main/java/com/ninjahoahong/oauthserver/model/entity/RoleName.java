package com.ninjahoahong.oauthserver.model.entity;


public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}