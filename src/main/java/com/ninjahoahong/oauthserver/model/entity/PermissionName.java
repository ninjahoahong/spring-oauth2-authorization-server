package com.ninjahoahong.oauthserver.model.entity;


public enum PermissionName {
    PERMISSION_MANAGEMENT,
    PERMISSION_BASIC
}
