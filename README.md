# Oauth2 Authorization Server

This is and authorization server build with Spring framework + Gradle build tool + Postgresql database. 
The authorization server is providing JWT access token. 

## What in the examples
* Provide JWT
* Auto database migration with Flyway
* Build with gradle
* Running locally with Docker + docker-compose
* Store data in Postgresql database
* Init database with startup sql-script

## Development Setup

1. Install Intellij-Idea-CE
2. Import project
3. Set up run configuration

## Build
`docker-compose up -d --build`

run only postgresql: `docker-compose up -d postgres`

clean up: `docker-compose down -v --rmi all --remove-orphans`
then access postgresql db: `docker exec -it app-postgres psql -h localhost -U user -d postgres`

## Test
`./gradlew test`

Example query using `grant_type=password`
```
curl -X POST \
  http://localhost:8081/oauth/token \
  -F grant_type=password \
  -F username=admin \
  -F password=password \
  -F client_id=client_id1 \
  -F client_secret=password
```

## Useful commands
* `docker-compose up -d --build`
* `keytool -genkeypair -alias jwt -keyalg RSA -keypass password -keystore dev.jks -storepass password`
* `keytool -importkeystore -srckeystore jwt.jks -destkeystore dev.jks -deststoretype pkcs12`
* `keytool -list -rfc --keystore dev.jks | openssl x509 -inform pem -pubkey`
* `docker exec -it app-postgres psql -h localhost -U user -d postgres`

## Dependencies
* Spring boot
* Spring security
* Postgresql 
* Flyway
* Lombok

## Resources

1. [Spring guides](https://spring.io/guides)
2. [Beadung site](https://www.baeldung.com/) 
3. [Ruslan's Spring template with maven](https://github.com/huksley/spring-boot2-template)
4. [Talk2 Amareswaran's tutorial](https://www.youtube.com/watch?v=wxebTn_a930)